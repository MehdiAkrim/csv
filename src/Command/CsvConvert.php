<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CsvConvert extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'Csv-Convert';
    protected function read($url)
    {
        $file = fopen($url, 'r');
        while (!feof($file)) {
            $line[] = fgetcsv($file, 2048,';');
        }
        fclose($file);
        return $line;
    }
    protected function dateformat($table) {
        for($i=1;$i<sizeof($table);$i++){
            $date=date_create($table[$i][6]);
            $table[$i][6]=date_format($date,"l, d-M-y h:m:s e");
        }
        return $table;
    }
     protected function priceandslug($table) {
        // fusion of price and currency
        for($i=1;$i<sizeof($table);$i++) {
            if($i==1) {
                unset($table[0][4]);
            }
            //remplacement point par virgule
            $number=strval(number_format($table[$i][3],2,',',''));
            $table[$i][3] = $number.$table[$i][4];
            unset($table[$i][4]);
        }
        // créations du slug et des retour au lignes sur description
        for($i=0;$i<sizeof($table);$i++) {
            if($i==0) {
                $table[$i][1]='slug';
            }
            else{
                $table[$i][1]=str_replace(' ','_',$table[$i][1]);
                $table[$i][5]=str_replace('\r',"\n",$table[$i][5]);
                $table[$i][5]=str_replace('<br/>',"\n",$table[$i][5]);
                $table[$i][1]=preg_replace('/[^a-z0-9A-Z_-]/', '-', $table[$i][1]);
                $table[$i][1]=strtolower($table[$i][1]);
            }
        }
        return $table;
    }
    protected function status($table) {
        for($i=0;$i<sizeof($table);$i++){
            if($i==0) {
                $table[$i][2]="status";
            }
            else{
                if($table[$i][2]==1) {
                    $table[$i][2]='Enable';
                }
                if($table[$i][2]==0){
                    $table[$i][2]='Disable';
                }
            }
        }
        return $table;
    }

    protected function configure()
    {
        $this->addArgument('url',InputArgument::REQUIRED);
        $this->addArgument('json',InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io= new SymfonyStyle($input,$output);
        $result=$this->read($input->getArgument('url'));
        $result=$this->dateformat($result);
        $result=$this->priceandslug($result);
        $result=$this->status($result);
        $rows=$result;
        unset($rows[0]);
        if($input->getArgument('json')=='json') {
            $output->writeln(json_encode($result));
        } else {
            $io->table($result[0],$rows);
        }

        return Command::SUCCESS;
    }
}